Django==2.0.8
django-filter==2.0.0
django-model-utils==3.1.2
djangorestframework==3.8.2
psycopg2==2.7.5
pytz==2018.5

from rest_framework.test import APITestCase


class TestList(APITestCase):
    """Тесты PagesViewSet."""

    def test_list(self):
        """Тест, что при запросе списка страниц возвращаются все страницы."""
        url = 'http://testserver/pages/'
        response = self.client.get(url)
        assert response.status_code == 200
        assert response.data['count'] == 20

    def test_retrieve(self):
        """Тест, что при запросе страницы возвращается весь контент."""
        url = 'http://testserver/pages/1/'
        response = self.client.get(url)
        assert response.status_code == 200
        assert response.data['id'] == 1
        assert len(response.data['members']) == 9

    def test_counter(self):
        """Тест, что при показе страницы увеличивается счетчик контента."""
        url = 'http://testserver/pages/1/'
        response = self.client.get(url)
        assert response.status_code == 200
        for content in response.data['members']:
            assert content['counter'] == 1

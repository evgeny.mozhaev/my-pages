from django.db import transaction
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response

from pages.models import Page
from pages import serializers


class PagesViewSet(ReadOnlyModelViewSet):
    """Контроллер модели Page.

    Позволяет получать список страниц и детальную информацию о странице.
    В детальной информации возвращает информацию о всех объектах контента,
     связанных со страницей.
    """
    queryset = Page.objects.all()

    def get_serializer_class(self):
        """Возвращает подходящий запросу сериалайзер.

        action == 'list' - запрос на получение списка страниц.
        action == 'retrieve' - запрос на получение детальной информации
        о странице.
        """
        if self.action == 'list':
            return serializers.PageListSerializer
        if self.action == 'retrieve':
            return serializers.PageDetailSerializer

    @transaction.atomic
    def retrieve(self, request, *args, **kwargs):
        """Возвращает детальную информацию о странице.

        Возвращает информацию о всех объектах контента, связанных со страницей.
        Вызывает метод увеличения счетчиков контента.
        """
        instance = self.get_object()
        instance._increment_contents_counters()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

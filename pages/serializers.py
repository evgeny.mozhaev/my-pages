from rest_framework import serializers

from pages.models import Page, Video, Audio, Text, Content


class PageListSerializer(serializers.HyperlinkedModelSerializer):
    """Сериалайзер списка страниц."""
    class Meta:
        model = Page
        fields = ('url',)


class AudioSerializer(serializers.ModelSerializer):
    """Сериалайзер модели аудио."""
    class Meta:
        model = Audio
        fields = '__all__'


class VideoSerializer(serializers.ModelSerializer):
    """Сериалайзер модели видео."""
    class Meta:
        model = Video
        fields = '__all__'


class TextSerializer(serializers.ModelSerializer):
    """Сериалайзер модели текста."""
    class Meta:
        model = Text
        fields = '__all__'


class ContentSerializer(serializers.ModelSerializer):
    """Сериалайзер модели контента."""
    class Meta:
        model = Content
        fields = '__all__'


class ContentsListingField(serializers.RelatedField):
    """Сериалайзер поля страницы, содержащего список контента."""
    def to_representation(self, value):
        if type(value) is Audio:
            serializer = AudioSerializer(value)
        elif type(value) is Video:
            serializer = VideoSerializer(value)
        elif type(value) is Text:
            serializer = TextSerializer(value)
        else:
            serializer = ContentSerializer(value)
        return serializer.data


class PageDetailSerializer(serializers.ModelSerializer):
    """Сериалайзер детальной информации о странице."""
    members = ContentsListingField(many=True, read_only=True)

    class Meta:
        model = Page
        fields = '__all__'

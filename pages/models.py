"""
Модели приложения.

Используется библиотека django-model-utils для удобного обращения
к моделям-наследникам из родительской модели.
"""

from django.db import models
from model_utils.managers import InheritanceManager


class MyInheritanceManager(InheritanceManager):
    """Позволяет передавать сериалайзеру объекты Audio, Video, Text.

    Иначе в сериалайзер передаются объекты Content.
    """
    def all(self, *args, **kwargs):
        return self.select_subclasses(*args, **kwargs)


class Content(models.Model):
    """Родительская модель контента.

    Нужна для простоты связи контента со страницой. Предполагается,
     что экземпляры модели Content самостоятельно, без дочерних
     моделей, создаваться не будут.
    Методы:
     _increment_counter - увеличивает счетчик экземпляра модели на 1.
    """
    title = models.CharField(max_length=200, default='sometitle')
    counter = models.IntegerField(default=0)
    objects = MyInheritanceManager()

    def __str__(self):
        return self.title

    def _increment_counter(self):
        """увеличивает счетчик экземпляра модели на 1."""
        self.counter += 1
        self.save()


class Video(Content):
    """Модель видео контента."""
    file_link = models.FilePathField(
        path='/home/evgeny/Work/justwork/task/task/contents/storage/files'
    )
    subtitles_link = models.FilePathField(
        path='/home/evgeny/Work/justwork/task/task/contents/storage/subtitles'
    )


class Audio(Content):
    """Модель аудио контента."""
    bitrate = models.IntegerField(default=1)


class Text(Content):
    """Модель текстового контента."""
    text_field = models.TextField(default='some text')


class Page(models.Model):
    """Модель страницы.

    Методы:
     _increment_contents_counters - увеличивает на 1 счетчик
     объектов контента, привязанных к странице.
    """
    title = models.CharField(max_length=200)
    members = models.ManyToManyField(Content, through='Membership')

    def __str__(self):
        return self.title

    def _increment_contents_counters(self):
        """Увеличивает на 1 счетчик объектов контента, привязанных к странице."""
        contents = self.members.all()
        for obj in contents:
            obj._increment_counter()
            obj.save()


class Membership(models.Model):
    """Модель связи `страница - контент`.

    Позволяет добавлять на страницу один объект контента более одного раза.
    """
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    content = models.ForeignKey(Content, on_delete=models.CASCADE)

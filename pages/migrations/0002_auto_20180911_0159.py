# Generated by Django 2.1.1 on 2018-09-10 22:59

from django.db import migrations


def create_initial_data(apps, schema_editor):
    Page = apps.get_model('pages', 'Page')
    Audio = apps.get_model('pages', 'Audio')
    Video = apps.get_model('pages', 'Video')
    Text = apps.get_model('pages', 'Text')
    Membership = apps.get_model('pages', 'Membership')
    pages = [Page.objects.create(title='page {}'.format(n)) for n in range(20)]
    for n in range(3):
        video = Video.objects.create(title='video {}'.format(n))
        audio = Audio.objects.create(title='audio {}'.format(n))
        text = Text.objects.create(title='text {}'.format(n), text_field='some text {}'.format(n))
        for page in pages:
            Membership.objects.create(page=page, content=video)
            Membership.objects.create(page=page, content=audio)
            Membership.objects.create(page=page, content=text)


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_initial_data),
    ]

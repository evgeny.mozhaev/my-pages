from django.contrib import admin

from pages.models import Page, Membership, Audio, Video, Text


class MembershipInline(admin.TabularInline):
    model = Membership
    extra = 1


class PageAdmin(admin.ModelAdmin):
    inlines = [MembershipInline]
    search_fields = ['title__startswith']


class AudioAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']


class VideoAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']


class TextAdmin(admin.ModelAdmin):
    search_fields = ['title__startswith']


admin.site.register(Page, PageAdmin)
admin.site.register(Audio, AudioAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Text, TextAdmin)

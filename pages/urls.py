from rest_framework import routers

from pages.views import PagesViewSet

router = routers.SimpleRouter()
router.register('', PagesViewSet)
urlpatterns = router.urls